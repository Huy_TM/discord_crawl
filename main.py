import discord
import pytz
import asyncio
from datetime import datetime
from discord.ext import commands
import time


intents = discord.Intents.all()
intents.message_content = True
bot = commands.Bot(command_prefix=".",intents=intents)
BOTTOKEN = '' # Discord TOKEN

@bot.event
async def on_message(message): 
    messageTime = message.created_at 
    current_time_utc = datetime.now(pytz.UTC)
    time_difference = current_time_utc - messageTime 
    print(message.channel)
    print(message.content)
    if message.attachments:
        for attachment in message.attachments:
            print(attachment.url)
    # print(message.attachments[0].url)
    await bot.process_commands(message)

@bot.command()
async def hello(ctx):
    print("command")
    await ctx.send("hello, I am a discrod bot")
    
bot.run(BOTTOKEN)